package org.net4care.xdsconnector;

import java.util.List;

import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.Utilities.RetrieveDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.w3c.dom.Document;

public interface IRepositoryConnector {

  RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder)
      throws SendRequestException;
  
  RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) throws SendRequestException;
  
  RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docIds) throws SendRequestException;

  RegistryResponseType provideAndRegisterCDADocument(Document cda,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode)
          throws PrepareRequestException, SendRequestException;

  RegistryResponseType provideAndRegisterCDADocument(String cda,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode)
          throws PrepareRequestException, SendRequestException;

  RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) 
          throws PrepareRequestException, SendRequestException;

}