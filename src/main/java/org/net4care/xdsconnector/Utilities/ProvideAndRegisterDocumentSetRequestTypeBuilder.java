package org.net4care.xdsconnector.Utilities;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.net4care.xdsconnector.service.ObjectFactory;
import org.net4care.xdsconnector.service.ProvideAndRegisterDocumentSetRequestType;
import org.net4care.xdsconnector.service.SubmitObjectsRequest;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ProvideAndRegisterDocumentSetRequestTypeBuilder {
  protected final Log logger = LogFactory.getLog(getClass());
  private String repositoryId; // optional on ProvideAndRegisterDocumentSet
  private String homeCommunityId; // optional on ProvideAndRegisterDocumentSet
  private String sourceId; // required for ProvideAndRegisterDocumentSet
  private ProvideAndRegisterMetadataMode mode = new ProvideAndRegisterMetadataMode(); // Default
  private ProvideAndRegisterDocumentSetRequestType request;

  public ProvideAndRegisterDocumentSetRequestTypeBuilder newInstance() {
    ProvideAndRegisterDocumentSetRequestTypeBuilder result = new ProvideAndRegisterDocumentSetRequestTypeBuilder();
    result.repositoryId = this.repositoryId;
    result.homeCommunityId = this.homeCommunityId;
    result.sourceId = this.sourceId;
    result.mode = this.mode;
    return result;
  }
  
  public ProvideAndRegisterDocumentSetRequestTypeBuilder setRequest(ProvideAndRegisterDocumentSetRequestType request) {
    this.request = request;
    return this;
  }

  public ProvideAndRegisterDocumentSetRequestType getRequest() {
    return request;
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder setRepositoryId(String repositoryId) {
    this.repositoryId = repositoryId;
    return this;
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder setHomeCommunityId(String homeCommunityId) {
    this.homeCommunityId = homeCommunityId;
    return this;
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder setSourceId(String sourceId) {
    this.sourceId = sourceId;
    return this;
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder setProvideAndRegisterMetadataMode(ProvideAndRegisterMetadataMode mode) {
    if (mode == null) {
      throw new IllegalArgumentException("ProvideAndRegisterMetadataMode cannot be null");
    }
    this.mode = mode;
    return this;
  }
  
  public JAXBElement<ProvideAndRegisterDocumentSetRequestType> getRequestPayload() {
    return new ObjectFactory().createProvideAndRegisterDocumentSetRequest(request);
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder buildProvideAndRegisterDocumentRequest(
    String cdaDocument,
    CodedValue healthcareFacilityTypeCode,
    CodedValue practiceSettingCode) throws ProvideAndRegisterDocumentSetRequestException, SubmitObjectsRequestException {
    return buildProvideAndRegisterDocumentRequest(Arrays.asList(cdaDocument), healthcareFacilityTypeCode, practiceSettingCode);
  }

  public ProvideAndRegisterDocumentSetRequestTypeBuilder buildProvideAndRegisterDocumentRequest(
    List<String> cdaDocumentList,
    CodedValue healthcareFacilityTypeCode,
    CodedValue practiceSettingCode) throws ProvideAndRegisterDocumentSetRequestException, SubmitObjectsRequestException {
    ProvideAndRegisterDocumentSetRequestType request = new ProvideAndRegisterDocumentSetRequestType();
    SubmitObjectsRequest submitRequest = new SubmitObjectsRequest();
    request.setSubmitObjectsRequest(submitRequest);
    try {
      List<IdDocumentPair> idDocumentPairs = addDocumentsToRequest(request, cdaDocumentList);
      request.setSubmitObjectsRequest(
        new SubmitObjectsRequestHelper(repositoryId, homeCommunityId)
        .setSourceId(sourceId)
        .setProvideAndRegisterMetadataMode(mode)
        .buildFromCDAs(idDocumentPairs, healthcareFacilityTypeCode, practiceSettingCode));
    } catch (ProvideAndRegisterDocumentSetRequestException | SubmitObjectsRequestException e) {
      logger.error(e.getMessage(), e);
      throw e;
    }
    setRequest(request);
    return this;
  }

  private List<IdDocumentPair> addDocumentsToRequest(
    ProvideAndRegisterDocumentSetRequestType request,
    List<String> cdaDocumentList)
        throws ProvideAndRegisterDocumentSetRequestException {
    List<IdDocumentPair> idDocumentPairs = new ArrayList<IdDocumentPair>();
    try {
      for (String cdaDoc : cdaDocumentList) {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        byte[] bytes = cdaDoc.getBytes(StandardCharsets.UTF_8);
        Document cdaDocument = builder.parse(new ByteArrayInputStream(bytes));
        String documentEntryId = CodeUtil.prefixUUID(UUID.randomUUID().toString());
        idDocumentPairs.add(new IdDocumentPair(documentEntryId, cdaDocument));

        ProvideAndRegisterDocumentSetRequestType.Document document = new ProvideAndRegisterDocumentSetRequestType.Document();
        document.setId(documentEntryId);
        document.setValue(bytes);
        request.getDocument().add(document);
      }
      return idDocumentPairs;
    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new ProvideAndRegisterDocumentSetRequestException(e);
    }
  }

}
