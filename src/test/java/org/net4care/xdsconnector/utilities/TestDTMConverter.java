package org.net4care.xdsconnector.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Test;
import org.net4care.xdsconnector.Utilities.DTMConverter;

public class TestDTMConverter {

  @Test
  public void dtmFromCdaDateTimeTimezonedString() throws Exception {
    Assert.assertEquals("20180108112339", new DTMConverter().convert("20180108122339+0100"));
  }

  @Test(expected=ParseException.class)
  public void dtmFromCdaDateTimeNoTimezoneString() throws Exception {
    new DTMConverter().convert("20180108122339");
  }

  @Test
  public void dtmFromDateTime() throws Exception {
    Assert.assertEquals("20180815215959",
      new DTMConverter().convert(new SimpleDateFormat("yyyyMMddHHmmssZ").parse("20180815235959+0200")));
  }
}
